<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/functions.php';






/**
 * @link http://wiki.cantico.fr/index.php/D%C3%A9finition_d%27un_th%C3%A8me_d%27ic%C3%B4nes_pour_Ovidentia
 */
function Demo_iconsListView($id = null)
{
	$W = Demo_widgetFactory();

	$listView = $W->ListView($id)->addClass('widget-configuration-panel');

	if (($I = bab_functionality::get('Icons')) === false) {
		return $listView;
	}
		
	$I->includeCss();

	$listView->addItem($W->Icon('Configuration du site', Func_Icons::APPS_PREFERENCES_SITE))
			 ->addItem($W->Icon('Param�tres de la messagerie', Func_Icons::APPS_PREFERENCES_MAIL_SERVER))
			 ->addItem($W->Icon('Options de l\'utilisateur', Func_Icons::APPS_PREFERENCES_USER))
			 ->addItem($W->Icon('Configuration des t�l�chargements vers le serveur', Func_Icons::PLACES_FOLDER))
			 ->addItem($W->Icon('Configuration des formats de date et heure', Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT))
			 ->addItem($W->Icon('Options de l\'agenda et des cong�s', Func_Icons::APPS_CALENDAR))
			 ->addItem($W->Icon('Configuration de l\'authentification', Func_Icons::APPS_PREFERENCES_AUTHENTICATION))
			 ->addItem($W->Icon('Configuration de l\'inscription', Func_Icons::APPS_DIRECTORIES))
			 ->addItem($W->Icon('Param�tres de l\'�diteur Wysiwyg', Func_Icons::APPS_PREFERENCES_WYSIWYG_EDITOR))
			 ->addItem($W->Icon('Configuration du moteur de recherche', Func_Icons::APPS_PREFERENCES_SEARCH_ENGINE))
			 ->addItem($W->Icon('Web services', Func_Icons::APPS_PREFERENCES_WEBSERVICES));

	 return $listView;
}



/**
 * 
 * @return Widget_Frame
 */
function Demo_icons()
{
	$W = Demo_widgetFactory();

	$frame = $W->Frame('conf')->setLayout($W->VBoxLayout());

	$frame->addItem($W->Title('Configuration panel: icons on left / 48px'));
	$frame->addItem(Demo_iconsListView()->addClass('icon-left-48')->addClass('icon-48x48')->addClass('icon-left'));

	$frame->addItem($W->Title('Configuration panel: icons on top / 32px'));
	$frame->addItem(Demo_iconsListView()->addClass('icon-top-32')->addClass('icon-32x32')->addClass('icon-top'));

	$frame->addItem($W->Title('Configuration panel: icons on left / 24px'));
	$frame->addItem(Demo_iconsListView()->addClass('icon-left-24')->addClass('icon-24x24')->addClass('icon-left'));

	$frame->addItem($W->Title('Configuration panel: icons on top / 16px'));
	$frame->addItem(Demo_iconsListView()->addClass('icon-top-16')->addClass('icon-16x16')->addClass('icon-top'));

	return $frame;
}

