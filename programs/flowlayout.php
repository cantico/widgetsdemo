<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';







/**
 * @return Func_Widgets
 */
function wdemo_widgetFactory()
{
	$W = bab_functionality::get('Widgets', false);
	$W->includeCss();

	return $W; 
}


//$W = instanciateWidgetFactory();
//$W->includeCss();



/**
 * Creates a label and line edit in an hbox layout.
 *
 * @param string	$id
 * @param string	$labelText
 * @param int		$size
 * @return Widget_Frame
 */
function my_LabelEdit($id, $labelText, $size = 20)
{
	global $W;
	$lineEdit = $W->LineEdit($id);
	
	return $W->Frame()
			->setLayout($W->VBoxLayout())
			->addItem($W->Label($labelText)->setAssociatedWidget($lineEdit))
			->addItem($lineEdit->setName('-')->setSize($size));
}


/**
 * Creates a demonstration listview populated with sample data.
 * 
 * @param string	$id
 * @return Widget_ListView
 */
function my_ListView($id)
{
	global $W;
	$listView = $W->ListView($id);

	$listView->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Publics'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Personnels'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'x-office-document.png', 'Document1.doc')->setTitle('Document1.doc|<b>Type</b>: Document Office|<b>Modifi� le</b>: 10/12/2007 15:37'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'audio-x-generic.png', 'Musique.mp3')->setTitle('Musique.mp3|<b>Type</b>: Document audio|<b>Modifi� le</b>: 11/12/2007 12:57'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Document html'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Un autre document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-pdf.png', 'Fichier pdf'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Un troisi�me document html'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Publics'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Personnels'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'x-office-document.png', 'Document1.doc')->setTitle('Document1.doc|<b>Type</b>: Document Office|<b>Modifi� le</b>: 10/12/2007 15:37'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'audio-x-generic.png', 'Musique.mp3')->setTitle('Musique.mp3|<b>Type</b>: Document audio|<b>Modifi� le</b>: 11/12/2007 12:57'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Document html'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Un autre document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-pdf.png', 'Fichier pdf'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Un troisi�me document html'));
	return $listView;
}



function my_userFrame()
{
	global $W;
	
	$frame = $W->Frame('user');
	return $frame->addClasses(array('Widget_Resizable'))->setLayout($W->VBoxLayout())
			->setName('user')
			->addItem(my_LabelEdit('firstname', 'First name:', 40)->setName('firstname')->setWidth('100%'))
			->addItem(my_LabelEdit('lastname', 'Last name:', 40)->setName('lastname'))
			->addItem(my_LabelEdit('function', 'Function:', 20)->setName('function'))
			->addItem($W->VBoxLayout()->addItem($W->Label('Resume:'))->addItem($W->Uploader('resume')->setAcceptedMimeTypes('text/html')->setName('resume')))
			->addItem(my_LabelEdit('age', 'Age:', 3)->setName('age'));
}


require_once 'configurationframe.php';

/*
$dialog = $W->Dialog('main_dialog')->setName('user_info')
			->setLayout($W->HBoxLayout()->setSpacing(10))
			->addItem(my_userFrame())

			->addItem($W->Frame('address')
							->setLayout($W->VBoxLayout('aaa'))
							->setName('address')
							->addItem($W->Label('Street :'))
							->addItem(my_ListView('list_view')->setView(Widget_ListView::VIEW_LIST))
							->addItem(my_ListView('icon_view')->setView(Widget_ListView::VIEW_ICONS))
					 )
			->addItem($configFrame);

$dialog->setObject('testObject');

$dialog->setData($_POST);


$canvas = $W->HtmlCanvas();
$lv = my_ListView('lv')->setView(Widget_ListView::VIEW_LIST);
*/
//$html = $lv->display($canvas);


/**
 * @link http://wiki.cantico.fr/index.php/D%C3%A9finition_d%27un_th%C3%A8me_d%27ic%C3%B4nes_pour_Ovidentia
 */
function Demo_ListView($id)
{
	global $W;

	$listView = $W->ListView($id)->addClass('widget-configuration-panel');

	if (($I = bab_functionality::get('Icons')) === false) {
		return $listView;
	}
		
	$I->includeCss();

	$listView->addItem($W->Icon('Configuration du site', Func_Icons::APPS_PREFERENCES_SITE))
			 ->addItem($W->Icon('Param�tres de la messagerie', Func_Icons::APPS_PREFERENCES_MAIL_SERVER))
			 ->addItem($W->Icon('Options de l\'utilisateur', Func_Icons::APPS_PREFERENCES_USER))
			 ->addItem($W->Icon('Configuration des t�l�chargements vers le serveur', Func_Icons::PLACES_FOLDER))
			 ->addItem($W->Icon('Configuration des formats de date et heure', Func_Icons::APPS_PREFERENCES_DATE_TIME_FORMAT))
			 ->addItem($W->Icon('Options de l\'agenda et des cong�s', Func_Icons::APPS_CALENDAR))
			 ->addItem($W->Icon('Configuration de l\'authentification', Func_Icons::APPS_PREFERENCES_AUTHENTICATION))
			 ->addItem($W->Icon('Configuration de l\'inscription', Func_Icons::APPS_DIRECTORIES))
			 ->addItem($W->Icon('Param�tres de l\'�diteur Wysiwyg', Func_Icons::APPS_PREFERENCES_WYSIWYG_EDITOR))
			 ->addItem($W->Icon('Configuration du moteur de recherche', Func_Icons::APPS_PREFERENCES_SEARCH_ENGINE))
			 ->addItem($W->Icon('Web services', Func_Icons::APPS_PREFERENCES_WEBSERVICES));
			 return $listView;
}


function Demo_LabelEdit($id, $labelText, $editSize = null)
{
	global $W;

	$color = $W->Create('Widget_Color')->setHueFromString($labelText, 0.80, 0.30);
	$options = Widget_Canvas::Options()->textColor('#'.$color->getHexa());
	
	$edit = $W->LineEdit($id);
	if (isset($editSize)) {
		$edit->setSize($editSize);
	}
	$label = $W->Label($labelText)->setAssociatedWidget($edit);
	return $W->Frame(null, $W->HBoxLayout())->setCanvasOptions($options)->addItem($label)->addItem($edit);
}


function Demo_CheckboxLabel($id, $labelText)
{
	global $W;
	$checkbox = $W->CheckBox($id);
	$label = $W->Label($labelText)->setAssociatedWidget($checkbox);
	return $W->HBoxLayout()->addItem($checkbox)->addItem($label)->addClass('widget-valign-middle');
}




function Demo_demo1()
{
	global $babBody, $W;

	$babBody->title = 'Demo 1: A mixed configuration dialog sample';


	$canvas = $W->HtmlCanvas();

	$configFrame = new Demo_ConfigurationFrame('conf');
	
	$configFrame->addSection('site-configuration', 'Configuration du site Ovidentia')
						->addItem(Demo_ListView('lview')->addClass('icon-left')->addClass('icon-left-48')->addClass('icon-48x48'));
						
	$configFrame->addSection('date-configuration', 'Configuration date et heure')
						->addItem(Demo_LabelEdit('long-date-format', 'Long date format:', 10))
						->addItem(Demo_LabelEdit('short-date-format', 'Short date format:', 10))
						->addItem(Demo_LabelEdit('time-format', 'Time format:', 6))
						->addItem($W->FlowLayout()
										->addItem(Demo_CheckboxLabel('size16x16', 'A checkbox'))
										->addItem(Demo_CheckboxLabel('size32x32', 'Another checkbox')))
						->addItem(Widget_Label('Short Comment'))
						->addItem($W->TextEdit()->setColumns(40));
	
	$configFrame->addSection('fonts', 'Fonts')
						->addItem(Demo_LabelEdit('title-font', 'Title font:', 20))
						->addItem(Demo_LabelEdit('body-font', 'Body font:', 20));
	
	$configFrame2 = new Demo_ConfigurationFrame('conf2');
	
	$configFrame2->addSection('date-time-format2', 'Date and time format')
						->addItem(Demo_LabelEdit('long-date-format2', 'Long date format:', 10))
						->addItem(Demo_LabelEdit('short-date-format2', 'Short date format:', 10))
						->addItem(Demo_LabelEdit('time-format2', 'Time format:', 6))
						->addItem(Widget_Label('Short Comment'))
						->addItem($W->TextEdit()->setValue('Hello wolrd !')->setColumns(40));
	
	$configFrame2->addSection('fonts2', 'Fonts')
						->addItem(Demo_LabelEdit('title-font2', 'Title font:', 20))
						->addItem(Demo_LabelEdit('body-font2', 'Body font:', 20))
						->addItem(Demo_CheckboxLabel('bold2', 'Bold'))
						->addItem(Demo_CheckboxLabel('italic2', 'Italic'));
				
	$all = $W->Frame()->setLayout($W->FlowLayout())->addItem($configFrame)->addItem($configFrame2);
				
	$html = $configFrame->display($canvas);
	
	
	// Adding raw html / javascript to the page.
	$babBody->babEcho($html);
	/*
	$babBody->babEcho('
		<button id="toggle_pos">Toggle position</button>
		&nbsp;
		<button id="size16">16x16</button>
		<button id="size24">24x24</button>
		<button id="size32">32x32</button>
		<button id="size48">48x48</button>
		<script type="text/javascript">
			
			$j("#toggle_pos").click(function() {
				$j("#lview,ul").toggleClass("icon-left-16").toggleClass("icon-48x48");
			});
			
			</script>
	');
	*/
}


function Demo_demo2()
{
	global $babBody, $W;

	$babBody->title = 'Demo 2: Another sample';
	
	$canvas = $W->HtmlCanvas();

	$configFrame = new Demo_ConfigurationFrame('conf');

	
	$configFrame->addSection('site-configuration1', 'Configuration panel: icon left / 48px')->addClass('icon-left-48')->addClass('icon-48x48')->addClass('icon-left')
						->addItem(Demo_ListView('lview1'));
	$configFrame->addSection('site-configuration2', 'Configuration panel: icon top / 32x')->addClass('icon-top-32')->addClass('icon-32x32')->addClass('icon-top')
						->addItem(Demo_ListView('lview2'));
	$configFrame->addSection('site-configuration3', 'Configuration panel: icon left / 24px')->addClass('icon-left-24')->addClass('icon-24x24')->addClass('icon-left')
						->addItem(Demo_ListView('lview3'));
	$configFrame->addSection('site-configuration4', 'Configuration panel: icon top / 16px')->addClass('icon-top-16')->addClass('icon-16x16')->addClass('icon-top')
						->addItem(Demo_ListView('lview4'));

						
	
	$accordion = $W->Accordions();
	
	$accordion->addPanel('Toto', Demo_ListView('lview5'));
	$accordion->addPanel('Un autre', Demo_ListView('lview6'));
	
	$configFrame->addSection('site-configuration5', 'Accordions')->addClass('icon-top-16')->addClass('icon-16x16')->addClass('icon-top')
						->addItem($accordion);
	
	
	$all = $W->Frame()->setLayout($W->FlowLayout())->addItem($configFrame);
	
	$html = $configFrame->display($canvas);

	$babBody->babEcho($html);
}



function Demo_demo3()
{
	global $babBody, $W;

	$babBody->setTitle('Demo 3: link and Business application');
	
	global $W;
	
	$canvas = $W->HtmlCanvas();
	
	$frame = $W->Frame('user');
	
	
	
	$link = $W->create('Widget_Link', 'Business application page', $GLOBALS['babAddonUrl'].'demo&idx=business')->addClass('preferences-contact-list');
	// $html = $link->display($canvas);
	$html = $frame->addClass('icon-left-48')->addClass('icon-48x48')->addClass('icon-left')->setLayout($W->FlowLayout())->addItem($link)->display($canvas);

	$babBody->babEcho($html);
	
}


/**
 * @param unknown_type $steps
 * @param unknown_type $width
 * @param unknown_type $method
 * @param unknown_type $c1
 * @return Widget_Html
 */
function Demo_colorFrame($steps, $width, $method, $c1)
{
	global $babBody, $W;

	$color = $W->create('Widget_Color');

	$stepWidth = $width / $steps;
	$html = '';
	for ($v = 0; $v < 1; $v += 1/$steps) {
		$html .= '<div style="clear: left; float: left; width: '. ($width / 8). 'px; height: 1px"></div>';
		for ($u = 0; $u < 1; $u += 1/$steps) {

			$color->$method($c1, $u, $v);
			$html .= '<div style="float: left; width: ' . $stepWidth . 'px; height: ' . $stepWidth . 'px; padding: 0; color: #fff; background-color:#'. $color->getHexa().'">&nbsp;</div>';
		}
	}
	return $W->create('Widget_Html', $html);
}


function Demo_randomColorFrame($steps, $width, $L)
{
	global $babBody, $W;

	$color = $W->create('Widget_Color');

	$stepWidth = $width / $steps;
	$html = '';
	for ($v = 0; $v < 1; $v += 1/$steps) {
		$html .= '<div style="clear: left; float: left; width: '. ($width / 8). 'px; height: 1px"></div>';
		for ($u = 0; $u < 1; $u += 1/$steps) {

			$color->setHueFromString(uniqid(), 1, $L);
			$html .= '<div style="float: left; width: ' . $stepWidth . 'px; height: ' . $stepWidth . 'px; padding: 0; color: #fff; background-color:#'. $color->getHexa().'">&nbsp;</div>';
		}
	}
	return $W->create('Widget_Html', $html);
}








function Demo_color()
{
	global $babBody, $W;
	$canvas = $W->HtmlCanvas();
	
	$babBody->setTitle('Color Demo: using Widget_Color');
	
	$color = $W->create('Widget_Color');
	
	$steps = 24;
	$width = 120;
	
	$stepWidth = $width / $steps;

	$configFrame = new Demo_ConfigurationFrame('color');

	
	$frame = $W->Frame()->setLayout($W->FlowLayout());
	for ($y = 0.75; $y <= 1; $y += 0.2) {
		$colorWidget = Demo_colorFrame($steps, $width, 'setYUV', $y);
		$frame->addItem($W->VBoxLayout()
						->addItem($W->Label('Y ' . $y))
						->addItem($colorWidget)
						);
	}
	$configFrame->addSection('yuv', 'YUV Color Space')
						->addItem($frame);
	
	$frame = $W->Frame()->setLayout($W->FlowLayout());
	for ($r = 0.25; $r <= 1; $r += 0.4) {
		$colorWidget = Demo_colorFrame($steps, $width, 'setRGB', $r);
		$frame->addItem($W->VBoxLayout()
						->addItem($W->Label('R ' . $r))
						->addItem($colorWidget)
						);
	}
	$configFrame->addSection('rgb', 'RGB Color Space')
						->addItem($frame);


	$frame = $W->Frame()->setLayout($W->FlowLayout());
	for ($h = 0.25; $h <= 1; $h += 0.5) {
		$colorWidget = Demo_colorFrame($steps, $width, 'setHSL', $h);
		$frame->addItem($W->VBoxLayout()
						->addItem($W->Label('H ' . $h))
						->addItem($colorWidget)
						);
	}
	$configFrame->addSection('hsl', 'HSL Color Space')
						->addItem($frame);
						
						
						
						
	$frame = $W->Frame()->setLayout($W->FlowLayout());
	for ($L = 0.35; $L <= 1; $L += 0.3) {
		$colorWidget = Demo_randomColorFrame($steps, $width, $L);
		$frame->addItem($W->VBoxLayout()
						->addItem($W->Label('L ' . $L))
						->addItem($colorWidget)
						);
	}
	$configFrame->addSection('rand', 'setHueFromString(uniqid(), 1, L)')
						->addItem($frame);
						
						
						
	$babBody->babEcho($configFrame->display($canvas));

/*
	$babBody->babEcho('<div style="clear: both; font-size: 8pt"><big>RGB</big>');
		for ($y = 0.3; $y <= 1; $y += 0.5) {
		$babBody->babEcho('<div style="clear: both">R ' . $y . '</div>');
		for ($u = 0; $u <= 1; $u += 1/4) {
			$babBody->babEcho('<div style="float: left; width: ' . ($width / 4) .'px; padding: 0; color: #000; background-color: #fff; text-align: center">' .  $u * 256 . '</div>');
		}
		$babBody->babEcho('<br style="clear: both" />');
		for ($v = 0; $v < 1; $v += 1/$steps) {
			$babBody->babEcho('<div style="clear: left; float: left; width: '. ($width / 8). 'px; height: 1px"></div>');
			for ($u = 0; $u < 1; $u += 1/$steps) {
				$color->setRGB(round($y * 256), round($u*256), round($v*256));
				$babBody->babEcho('<div style="float: left; width: ' . $stepWidth . 'px; height: ' . $stepWidth . 'px; padding: 0; color: #fff; background-color:#'. $color->getHexa().'">&nbsp;</div>');
			}
		}
	}
	$babBody->babEcho('</div>');
	
	$babBody->babEcho('<div style="clear: both; font-size: 8pt"><big>HSL</big>');
	for ($s = 0.25; $s <= 1; $s += 0.4) {
		$babBody->babEcho('<div style="clear: both">Saturation ' . $s . '</div>');
		for ($h = 0; $h <= 1; $h += 1/4) {
			$babBody->babEcho('<div style="float: left; width: ' . ($width / 4) .'px; padding: 0; color: #000; background-color: #fff; text-align: center">' .  $h * 360 . '</div>');
		}
		$babBody->babEcho('<br style="clear: both" />');
		for ($v = 0; $v < 1; $v += 1/$steps) {
			$babBody->babEcho('<div style="clear: left; float: left; width: '. ($width / 8). 'px; height: 1px"></div>');
			for ($h = 0; $h < 1; $h += 1/$steps) {
				$color->setHSL(round($h * 360), round($s*100), round($v*100));
				$babBody->babEcho('<div style="float: left; width: ' . $stepWidth . 'px; height: ' . $stepWidth . 'px; padding: 0; color: #fff; background-color:#'. $color->getHexa().'">&nbsp;</div>');
			}
		}
	}
	$babBody->babEcho('</div>');
*/
}





function Demo_business() {

	global $W;
	
	$layout = $W->create('Widget_BusinessAppLayout');
	$page = $W->create('Widget_BusinessApplicationPage',null, $layout);
	$page->setJavascriptToolkit('jquery');
	
	
	$page->setTitle('Business Application test page');
	
	$page->setPortalLink('?');
	
	
	
	
	if (bab_rp('theme')) {
		$page->addStyleSheet('http://www.filamentgroup.com/examples/RollAUI/jquery-ui-themeroller.'.bab_rp('theme').'.css');
	} else {
		$page->addStyleSheet(bab_getAddonInfosInstance('widgets')->getStylePath().'default/theme.css');
	}
	
	$options = array(
		
		'smoothness' => 'smoothness',
		'excitebike' => 'excitebike',
		'southstreet' => 'southstreet',
		'blacktie' => 'blacktie',
		'creamsicle' => 'creamsicle',
		'corpcool' => 'corpcool',
		'mintchoco' => 'mintchoco',
		'dustyshelf' => 'dustyshelf',
		'dotluv' => 'dotluv',
		'uisite' => 'uisite',
		'cupertino' => 'cupertino'
	);
	
	$section = $page->createSection();
	$section->addItem(
		$W->Form(null, $W->VBoxLayout())
			->setReadOnly()
			->addItem($W->create('Widget_Select', null)->setName('theme')->setOptions($options))
			->addItem($W->SubmitButton()->setLabel('Apply'))
			->setSelfPageHiddenFields()
			->setValues($_GET)
	);
	
	
	
	
	
	
	
	$page->addRailItem('test1', 'test1', '?')->addRailItem('test2', 'test2', '?');
	
	$page->addItemMenu('onglet1', 'Onglet 1', '?')->addItemMenu('onglet2', 'Onglet 2', '?')->addItemMenu('onglet3', 'Onglet 3', '?');
	$page->setCurrentItemMenu('onglet2');
	
	
	$div = $page->createFrame();
	
	
	// external widgets
	
	$upload = @bab_functionality::get('FileUploader/ImageUploader');
	if (false !== $upload) {
		$upload->setFileUid('WidgetDemo_business');

		$multipleup = @bab_functionality::get('FileListUploader');
		if (false !== $multipleup) {
			$multipleup->setListUid('WidgetDemo_business_multipleup');
			$div->addItem(
				$W->HBoxItems($upload->getWidget(), $multipleup->getWidget() )
			);
		}
	}
	

	// multifield demo
	// use ->setJavascriptToolkit('jquery') on widget page
	
	$div
	->addItem(

		$W->HBoxItems(

			$W->Pair(
				$W->Label('LineEdit'),
				$W->MultiField()
				->setName('multifield_LineEdit')
				->addItem($W->LineEdit()->setName('0'))
				->addItem($W->LineEdit()->setName('1'))
				->addItem($W->LineEdit()->setName('2'))
			),

			$W->Pair(
				$W->Label('Select'),
				$W->MultiField()
				->setName('multifield_Select')
				->addItem($W->Select()->setOptions(array('1' => 'one', '2' => 'two'))->setName('0'))
			)

		)
	);

	
	
	
	$section = $page->createSection('Demo liens');
	$section->addItem($page->html_links(array(
		array(
			'title' => 'link 1',
			'url' => '?'
		),
		array(
			'title' => 'link 2',
			'url' => '?',
			'classname' => 'selected'
		),
		array(
			'title' => 'link 3',
			'url' => '?'
		),
		array(
			'title' => 'sublink 3',
			'url' => '?',
			'classname' => 'sublink'
		)
	)));
	
	
	
	$section = $page->createSection('Demo paires');
	
	$section->addItem(
		$W->create('Widget_Pair')
		->setFirst($W->create('Widget_Label', 'titre'))
		->setSecond($W->create('Widget_Label', 'valeur'))
	);
	
	
	$section->addItem(
		$W->create('Widget_Frame', null, $W->create('Widget_PairVBoxLayout', null, 'liste de test'))
		->addItem(
			$W->create('Widget_Pair')
			->setFirst($W->create('Widget_Label', 'titre 1'))
			->setSecond($W->create('Widget_Label', 'valeur 1'))
		)
		->addItem(
			$W->create('Widget_Pair')
			->setFirst($W->create('Widget_Label', 'titre 2'))
			->setSecond($W->create('Widget_Label', 'valeur 2'))
		)
		->addItem(
			$W->create('Widget_Label', 'tout seul')
		)
		->addItem(
			$W->create('Widget_Pair')
			->setFirst($W->create('Widget_Label', 'titre 3'))
			->setSecond($W->create('Widget_Label', 'valeur 3'))
		)
	);
	
	$section->addItem(
		$page->html_trList(array(
			
			array(
				'title' => 'titre 1',
				'value' => 'valeur 1'
			),
			array(
				'title' => 'titre 2',
				'value' => 'valeur 2',
				'classname' => 'money'
			)
		
		), $caption = 'liste de test 2')
	);
	
	
	global $babDB;
	$res = $babDB->db_query('SELECT * FROM bab_upgrade_messages');
	
	
	$div = $page->createFrame('List demo')->addItem(
		$W->create('Widget_BusinessApplicationList')->sqlHelper($res, array('bab_upgrade_messages'))
	);

	
	
	
	
	
	bab_debug($page->dump());

	$page->displayHtml();

}



$idx = bab_rp('idx', 'demo1');


switch ($idx)
{
	case 'demo1':
		Demo_demo1();
		break;
		
	case 'demo2':
		bab_requireCredential('L\'onglet Demo2 est une zone d�militaris�e. Veuillez vous authentifier.');
		// If php reaches this point, then the user is authenticated.
		Demo_demo2();
		break;

	case 'demo3':
		Demo_demo3();
		break;
		
	case 'color_demo':
		Demo_color();
		break;
		
	case 'business':
		Demo_business();
		break;
}

$babBody->addItemMenu('demo1', 'Demo 1', $GLOBALS['babAddonUrl'].'demo&idx=demo1');
$babBody->addItemMenu('demo2', 'Demo 2', $GLOBALS['babAddonUrl'].'demo&idx=demo2');
$babBody->addItemMenu('demo3', 'Demo 3', $GLOBALS['babAddonUrl'].'demo&idx=demo3');
$babBody->addItemMenu('color_demo', 'Color Demo', $GLOBALS['babAddonUrl'].'demo&idx=color_demo');
$babBody->addItemMenu('business', 'Business', $GLOBALS['babAddonUrl'].'demo&idx=business');
$babBody->setCurrentItemMenu($idx);




