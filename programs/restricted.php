<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';

bab_requireCredential();


if (!bab_isUserAdministrator())
{
	$babBody->addError('Access restricted to administrators. Please log in.');
	return;
}

/**
 * @return ui_widgets
 */
function instanciateWidgetFactory()
{
	return bab_functionality::get('Widgets');
}


$W = instanciateWidgetFactory();
$W->includeCss();
$W->includeJs();

/**
 * Creates a label and line edit in an hbox layout.
 *
 * @param string	$id
 * @param string	$labelText
 * @param int		$size
 * @return Widget_Frame
 */
function my_LabelEdit($id, $labelText, $size = 20)
{
	global $W;
	$lineEdit = $W->LineEdit($id);

	return $W->Frame()
			->setLayout($W->VBoxLayout())
			->addItem($W->Label($labelText)->setAssociatedWidget($lineEdit))
			->addItem($lineEdit->setName('-')->setSize($size));
}


/**
 * Creates a demonstration listview populated with sample data.
 * 
 * @param string	$id
 * @return Widget_ListView
 */
function my_ListView($id)
{
	global $W;
	$listView = $W->ListView($id);

	$listView->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Publics'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Personnels'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'x-office-document.png', 'Document1.doc')->setTitle('Document1.doc|<b>Type</b>: Document Office|<b>Modifi� le</b>: 10/12/2007 15:37'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'audio-x-generic.png', 'Musique.mp3')->setTitle('Musique.mp3|<b>Type</b>: Document audio|<b>Modifi� le</b>: 11/12/2007 12:57'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Document html'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Un autre document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-pdf.png', 'Fichier pdf'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Un troisi�me document html'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Publics'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'folder.png', 'Documents Personnels'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'x-office-document.png', 'Document1.doc')->setTitle('Document1.doc|<b>Type</b>: Document Office|<b>Modifi� le</b>: 10/12/2007 15:37'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'audio-x-generic.png', 'Musique.mp3')->setTitle('Musique.mp3|<b>Type</b>: Document audio|<b>Modifi� le</b>: 11/12/2007 12:57'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Document html'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-msword.png', 'Un autre document Word'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'gnome-mime-application-pdf.png', 'Fichier pdf'))
			 ->addItem($W->Icon($GLOBALS['babSkinPath'] . 'images/' . $GLOBALS['babAddonHtmlPath'] . 'text-html.png', 'Un troisi�me document html'));
	return $listView;
}



function my_userFrame()
{
	global $W;
	
	$frame = $W->Frame('user');
	return $frame->setLayout($W->VBoxLayout())
			->setName('user')
			->addItem(my_LabelEdit('firstname', 'First name:', 40)->setName('firstname'))
			->addItem(my_LabelEdit('lastname', 'Last name:', 40)->setName('lastname'))
			->addItem(my_LabelEdit('function', 'Function:', 20)->setName('function'))
			->addItem($W->VBoxLayout()->addItem($W->Label('Resume:'))->addItem($W->Uploader('resume')->setAcceptedMimeTypes('text/html')->setName('resume')))
			->addItem(my_LabelEdit('age', 'Age:', 3)->setName('age'));
}



$dialog = $W->Dialog('main_dialog')->setName('user_info')
			->setLayout($W->HBoxLayout()->setSpacing(10))
			->addItem(my_userFrame())

			->addItem($W->Frame('address')
						->setLayout($W->VBoxLayout('aaa'))
						->setName('address')
						->addItem($W->Label('Street :'))
						->addItem(my_ListView('list_view')->setView(Widget_ListView::VIEW_LIST))
						->addItem(my_ListView('icon_view')->setView(Widget_ListView::VIEW_ICONS))
					)
			;

$dialog->setObject('testObject');

$dialog->setData($_POST);


$babBody->babEcho($dialog->toHtml());




