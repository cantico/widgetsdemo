<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlCalendarDemo extends widgetsDemo_Controller
{

    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Resource calendar'),
                $W->VBoxItems(
                    $this->resourceCalendar()
                ),
            3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlCalendarDemo::resourceCalendar'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Calendar'),
                $W->VBoxItems(
                    $this->calendar2()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlCalendarDemo::calendar2'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Calendar'),
                $W->VBoxItems(
                    $this->calendar3()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlCalendarDemo::calendar3'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Calendar'),
                $W->VBoxItems(
                    $this->calendar()
                ),
                3
            )->setFoldable(true, true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlCalendarDemo::calendar'));


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Linked calendars'),
                $W->VBoxItems(
                    $this->linkedCalendars()
                ),
                3
            )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlCalendarDemo::linkedCalendars'));

        return $box;
    }



    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('Calendars'));

        return $page;
    }





    /**
     * Calendar demo.
     *
     * @return Widget_VBoxLayout
     */
    public function calendar()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        // A simple calendar.
        //---------------------------------------------------------------------
        $calendar = $W->FullCalendar();
        $calendar->setView(Widget_FullCalendar::VIEW_MONTH);
        $calendar->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));
        $box->addItem(
            $calendar
        );

        return $box;
    }



    /**
     * Calendar demo.
     *
     * @return Widget_VBoxLayout
     */
    public function linkedCalendars()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A simple calendar.
        //---------------------------------------------------------------------
        $calendar1 = $W->FullCalendar();
        $calendar1->setView(Widget_FullCalendar::VIEW_WEEK);
        $calendar1->hideWeekends();
        $calendar1->setHeaderCenter('');
        $calendar1->setHeaderLeft(Widget_FullCalendar::TITLE);
        $calendar1->setHeaderRight(' ');
        $calendar1->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));
        $calendar1->setMetadata('contentHeight', '600');

        $calendar2 = $W->FullCalendar();
        $calendar2->setView(Widget_FullCalendar::VIEW_WEEK);
        $calendar2->hideWeekends();
        $calendar2->setHeaderCenter('B');
        $calendar2->setHeaderLeft(' ');
        $calendar2->setHeaderRight(' ');
        $calendar2->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));
        $calendar2->setMetadata('contentHeight', '600');

        $calendar3 = $W->FullCalendar();
        $calendar3->setView(Widget_FullCalendar::VIEW_WEEK);
        $calendar3->hideWeekends();
        $calendar3->setHeaderCenter('C');
        $calendar3->setHeaderLeft(' ');
        $calendar3->setHeaderRight(' ');
        $calendar3->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));
        $calendar3->setMetadata('contentHeight', '600');

        $previousButton = $W->Label('Prev')->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_GO_PREVIOUS);
        $nextButton = $W->Label('Next')->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_GO_NEXT);
        $weekButton = $W->Label('Week')->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK);
        $monthButton = $W->Label('Month')->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH);

        $calendar1->setNextButton($nextButton);
        $calendar2->setNextButton($nextButton);
        $calendar3->setNextButton($nextButton);

        $calendar1->setPreviousButton($previousButton);
        $calendar2->setPreviousButton($previousButton);
        $calendar3->setPreviousButton($previousButton);

        $calendar1->setWeekViewButton($weekButton);
        $calendar2->setWeekViewButton($weekButton);
        $calendar3->setWeekViewButton($weekButton);

        $calendar1->setMonthViewButton($monthButton);
        $calendar2->setMonthViewButton($monthButton);
        $calendar3->setMonthViewButton($monthButton);

        $box->addItem(
            $W->FlowItems(
                $W->FlowItems($previousButton, $nextButton)->addClass(Func_Icons::ICON_LEFT_SYMBOLIC),
                $W->FlowItems($weekButton, $monthButton)->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em')
        );
        $box->addItem(
            $W->HBoxItems(
                $calendar1->setSizePolicy('widget-33pc'),
                $calendar2->setSizePolicy('widget-33pc'),
                $calendar3->setSizePolicy('widget-33pc')
            )->addClass('widget-100pc')
            ->setHorizontalSpacing(1, 'em')
            ->setVerticalAlign('bottom')
        );

        return $box;
    }



    /**
     * Calendar demo.
     *
     * @return Widget_VBoxLayout
     */
    public function calendar2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // Simple calendars.
        //---------------------------------------------------------------------
        $calendar = $W->FullCalendar();
        $calendar->setView(Widget_FullCalendar::VIEW_WEEK);
        $calendar->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));

        $calendar2 = $W->FullCalendar();
        $calendar2->setView(Widget_FullCalendar::VIEW_MONTH);
        $calendar2->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));

        $box->addItem(
            $W->FlowItems(
                $calendar->setSizePolicy('widget-50pc'),
                $calendar2->setSizePolicy('widget-50pc')
            )->setSpacing(2, 'em')
        );

        return $box;
    }


    /**
     * Resource calendar demo.
     *
     * @return Widget_VBoxLayout
     */
    public function resourceCalendar()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // Resource calendars.
        //---------------------------------------------------------------------
        $calendar = $W->FullCalendar();
        $calendar->setView(Widget_FullCalendar::VIEW_RESOURCE_MONTH);

        $resources = array(
            array('id' => 'r1', 'name' => '<div><b>Resource 1</b><br>A resource</div>'),
            array('id' => 'r2', 'name' => '<div><b>Resource 2</b><br>A resource</div>'),
            array('id' => 'r3', 'name' => '<div><b>Resource 3</b><br>A resource</div>'),
        );
        $calendar->setResources($resources);
        $calendar->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));

        $calendar2 = $W->FullCalendar();
        $calendar2->setView(Widget_FullCalendar::VIEW_RESOURCE_MONTH);

        $calendar2->setResources($resources);
        $calendar2->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));

        $box->addItem(
            $W->VBoxItems(
                $calendar->setSizePolicy('widget-100pc'),
                $calendar2->setSizePolicy('widget-100pc')
            )
        );

        return $box;
    }


    /**
     * Calendar demo.
     *
     * @return Widget_VBoxLayout
     */
    public function calendar3()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // Simple calendars.
        //---------------------------------------------------------------------
        $calendar = $W->FullCalendar();
        $calendar->setView(Widget_FullCalendar::VIEW_MONTH);
        $calendar->setHeaderLeft(Widget_FullCalendar::BUTTON_PREVIOUS . ',' . Widget_FullCalendar::BUTTON_NEXT);
        $calendar->setHeaderCenter(' ');
        $calendar->setHeaderRight(Widget_FullCalendar::TITLE);
        $calendar->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));

        $calendar2 = $W->FullCalendar();
        $calendar2->setView(Widget_FullCalendar::VIEW_WORKWEEK);
        $calendar2->setHeaderLeft(Widget_FullCalendar::BUTTON_PREVIOUS);
        $calendar2->setHeaderCenter(Widget_FullCalendar::TITLE);
        $calendar2->setHeaderRight(Widget_FullCalendar::BUTTON_NEXT);
        $calendar2->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));

        $calendar3 = $W->FullCalendar();
        $calendar3->setView(Widget_FullCalendar::VIEW_DAY);
        $calendar3->setHeaderLeft('');

        $calendar3->setHeaderCenter(Widget_FullCalendar::TITLE);

        $calendar3->setHeaderLeft(Widget_FullCalendar::TITLE);
        $calendar3->setHeaderCenter(' ');
        $calendar3->setHeaderRight(Widget_FullCalendar::BUTTON_PREVIOUS . ',' . Widget_FullCalendar::BUTTON_NEXT);
        $calendar3->fetchPeriods($this->Event()->events('personal/' . bab_getUserId()));

        $box->addItem(
            $W->FlowItems(
                $calendar->setSizePolicy('widget-33pc'),
                $calendar2->setSizePolicy('widget-33pc'),
                $calendar3->setSizePolicy('widget-33pc')
            )->setSpacing(2, 'em')
        );

        return $box;
    }
}
