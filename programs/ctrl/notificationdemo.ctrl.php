<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';

require_once dirname(__FILE__) . '/../set/sample.class.php';


/**
 *
 */
class widgetsDemo_CtrlNotificationDemo extends widgetsDemo_Controller
{




    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Notifications'),
                $W->VBoxItems(
                    $this->notificationDemo1()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem(
            $this->codeSection(
                array(
                    'widgetsDemo_CtrlNotificationDemo::notificationDemo1'
                )
            )
        );


        return $box;
    }




    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('Notifications'));

        return $page;
    }






    public function showError()
    {
        $this->addError('An error message');

        return true;
    }


    public function showMessage()
    {
        $this->addMessage('A message');

        return true;
    }

    public function showSeveralMessages()
    {
        $this->addMessage('First message');
        $this->addMessage('Second message');
        $this->addError('An error message');
        $this->addMessage("Third message<br>Messages are in <b>html</b>.<br><br><i>Lorem ipsum...</i>");

        return true;
    }


    /**
     * Notification demo.
     *
     * @return Widget_VBoxLayout
     */
    public function notificationDemo1()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $W->Link(
                'Error notification',
                $this->proxy()->showError()
            )->setAjaxAction()
            ->addClass('widget-actionbutton')
        );

        $box->addItem(
            $W->Link(
                'Notification',
                $this->proxy()->showMessage()
            )->setAjaxAction()
            ->addClass('widget-actionbutton')
        );

        $box->addItem(
            $W->Link(
                'Many notifications',
                $this->proxy()->showSeveralMessages()
            )->setAjaxAction()
            ->addClass('widget-actionbutton')
        );


        return $box;
    }
}
