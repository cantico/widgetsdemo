<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlOrgChartDemo extends widgetsDemo_Controller
{



    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');


        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Sample local persistent organizational chart'),
                $W->VBoxItems(
                    $this->orgChart2()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlOrgChartDemo::orgChart2'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Sample session persistent organizational chart'),
                $W->VBoxItems(
                    $this->orgChart()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlOrgChartDemo::orgChart'));



        return $box;
    }



    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
        	return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('Organizational charts'));

        return $page;
    }



    /**
     *
     * @param Widget_OrgchartView $orgChart
     * @param string $id
     * @param string $name
     */
    private function createEntity(Widget_OrgchartView $orgChart, $id, $name)
    {
        $entity = $orgChart->createElement($id, 'entity', $name);

        $entity->addMember('Contact 1', 'Manager', '');
        $entity->addMember('Contact 2', 'Assistant', '');
        $entity->addMember('Contact 3', 'Assistant', '');

        $entity->addAction(
            'toggle_members',
            widgetsDemo_translate('Show/Hide entity members'),
            '',
            '',
            'toggleMembers',
            array('this'),
            Func_Icons::OBJECTS_GROUP . ' icon'
        );
        $entity->addAction(
            'hide_members',
            widgetsDemo_translate('Hide all entity members'),
            '',
            '',
            'closeAllMembers',
            array('this'),
            Func_Icons::ACTIONS_LIST_REMOVE . ' icon'
        );
        $entity->addAction(
            'show_members',
            widgetsDemo_translate('Show all entity members'),
            '',
            '',
            'openAllMembers',
            array('this'),
            Func_Icons::ACTIONS_LIST_ADD . ' icon'
        );

        return $entity;
    }


    /**
     * Org chart demo.
     *
     * @return Widget_VBoxLayout
     */
    public function orgChart()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A simple organizational chart.
        //---------------------------------------------------------------------
        $orgChart = $W->OrgchartView('widgetsdemo-orgchart1');

//        $orgChart->setPersistent(true, Widget_Widget::STORAGE_SESSION);


        $root = $this->createEntity($orgChart, 'root', 'Root');

        $entity1 = $this->createEntity($orgChart, 'entity2.1', 'Entity 1');
        $entity2 = $this->createEntity($orgChart, 'entity2.2', 'Entity 2');
        $entity3 = $this->createEntity($orgChart, 'entity2.3', 'Entity 3');
        $entity4 = $this->createEntity($orgChart, 'entity2.4', 'Entity 4');

        $entity1_1 = $this->createEntity($orgChart, 'entity2.1.1', 'Entity 1.1');
        $entity1_2 = $this->createEntity($orgChart, 'entity2.1.2', 'Entity 1.2');
        $entity1_3 = $this->createEntity($orgChart, 'entity2.1.3', 'Entity 1.3');
        $entity1_4 = $this->createEntity($orgChart, 'entity2.1.4', 'Entity 1.4');

        $orgChart->appendElement($root, null);
        $orgChart->appendElement($entity1, 'root');
        $orgChart->appendElement($entity2, 'root');
        $orgChart->appendElement($entity3, 'root');
        $orgChart->appendElement($entity4, 'root');
        $orgChart->appendElement($entity1_1, 'entity2.1');
        $orgChart->appendElement($entity1_2, 'entity2.1');
        $orgChart->appendElement($entity1_3, 'entity2.1');
        $orgChart->appendElement($entity1_4, 'entity2.1');

        $box->addItem(
        	$orgChart
        );

        return $box;
    }

    /**
     * Org chart demo.
     *
     * @return Widget_VBoxLayout
     */
    public function orgChart2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // A simple organizational chart.
        //---------------------------------------------------------------------
        $orgChart = $W->OrgchartView('widgetsdemo-orgchart2');

//        $orgChart->setPersistent(true, Widget_Widget::STORAGE_LOCAL);


//         $zoomFactor = $W->getUserConfiguration('orgchart/' . $orgChart->getRealId() . '/zoomFactor');
//         $openNodes = $W->getUserConfiguration('orgchart/' . $orgChart->getRealId() . '/openNodes');
//         $openMembers = $W->getUserConfiguration('orgchart/' . $orgChart->getRealId() . '/openMembers');
//         $verticalThreshold = $W->getUserConfiguration('orgchart/' . $orgChart->getRealId() . '/verticalThreshold');

//         var_dump($verticalThreshold);
//         $orgChart->setZoomFactor($zoomFactor);
//         $orgChart->setOpenNodes(explode(',', $openNodes));
//         $orgChart->setOpenMembers(explode(',', $openMembers));
//         $orgChart->setVerticalThreshold($verticalThreshold);

//        var_dump(explode(',', $openNodes));

        $orgChart->setAdminMode(true);

        $root = $this->createEntity($orgChart, 'root2', 'Root');

        $entity1 = $this->createEntity($orgChart, 'entity2.1', 'Entity 1');
        $entity2 = $this->createEntity($orgChart, 'entity2.2', 'Entity 2');
        $entity3 = $this->createEntity($orgChart, 'entity2.3', 'Entity 3');
        $entity4 = $this->createEntity($orgChart, 'entity2.4', 'Entity 4');

        $entity1_1 = $this->createEntity($orgChart, 'entity2.1.1', 'Entity 1.1');
        $entity1_2 = $this->createEntity($orgChart, 'entity2.1.2', 'Entity 1.2');
        $entity1_3 = $this->createEntity($orgChart, 'entity2.1.3', 'Entity 1.3');
        $entity1_4 = $this->createEntity($orgChart, 'entity2.1.4', 'Entity 1.4');


        $orgChart->appendElement($root, null);
        $orgChart->appendElement($entity1, 'root2');
        $orgChart->appendElement($entity2, 'root2');
        $orgChart->appendElement($entity3, 'root2');
        $orgChart->appendElement($entity4, 'root2');
        $orgChart->appendElement($entity1_1, 'entity2.1');
        $orgChart->appendElement($entity1_2, 'entity2.1');
        $orgChart->appendElement($entity1_3, 'entity2.1');
        $orgChart->appendElement($entity1_4, 'entity2.1');

        $functionPicker = $W->FunctionPicker(1);

        $box->addItem(
            $this->labelledItem('Function picker', $functionPicker)
        );

        $box->addItem(
            $this->labelledItem('Simple organizational chart (with threshold at level 3)', $orgChart)
        );

        return $box;
    }
}
