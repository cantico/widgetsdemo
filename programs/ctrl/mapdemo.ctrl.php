<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlMapDemo extends widgetsDemo_Controller
{



    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');



        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Maps'),
                $W->VBoxItems(
                    $this->maps()
                ),
                3
            )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlMapDemo::maps'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Maps (Google)'),
                $W->VBoxItems(
                    $this->maps2('google')
                ),
                3
            )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlMapDemo::maps2'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Maps (OSM/Leaflet)'),
                $W->VBoxItems(
                    $this->maps2('leafletjs')
                ),
            3
            )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlMapDemo::maps2'));

        return $box;
    }



    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('Map widgets'));

        return $page;
    }






    /**
     * Map demo.
     *
     * @return Widget_VBoxLayout
     */
    public function maps()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $map = $W->Map();
        $map->setMapFrom('google');

//        $map->setWidth(600);
//        $map->setHeight(450);

        $map->setCenter(45, 0);
        $map->setZoom(5);

        $box->addItem(
            $this->labelledItem('Map', $map)
        );
        return $box;
    }


    /**
     * Map demo.
     *
     * @return Widget_VBoxLayout
     */
    public function maps2($type)
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $map = $W->Map();
        $map->setMapFrom($type);

        $map->setWidth('100%');
        $map->setHeight('30em');

        $map->setCenter(45, 0);
        $map->setZoom(6);


        $map->addMarker(0, 0, '', $W->Label('You are here. Center of the world')->setId('marker1'));

        $map->addMarker(60, 8, '', $W->Label('You are not here.')->setId('marker2'));

        $link1 = $W->Link('Open marker 1', bab_url::get_request_gp()->toString() . '#' . $map->getId());

        $link2 = $W->Link('Open marker 2', bab_url::get_request_gp()->toString() . '#' . $map->getId());

        $box->addItem(
            $W->Label('A map with two markers and links to move to those markers.')
        );

        $box->addItem(
            $this->labelledItem('Map', $map)
        );

        $box->addItem($link1);

        $box->addItem($link2);

        $map->addMarkerOpener('marker1', $link1);
        $map->addMarkerOpener('marker2', $link2);

        return $box;
    }
}
