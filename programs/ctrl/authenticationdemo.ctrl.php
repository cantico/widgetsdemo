<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlAuthenticationDemo extends widgetsDemo_Controller
{	
    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
            return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle('Authentication');

        return $page;
    }
	
    
    
    
	
	public function demo()
	{
		$W = bab_Widgets();
		$layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
		
		$layout->addItem($W->Link('Restricted page link', $this->proxy()->loginRequired()));
		
		$layout->addItem(
			$W->Link(
				'Ajax restricted page popup',
				$this->proxy()->loginRequired()
			)->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
		);
		
		
		$layout->addItem($W->Title('Event on form'));
		
		$editor = $this->testEditor();
		$editor->setReloadAction($this->proxy()->testEditor());
		
		
		$layout->addItem($editor);
		
		
		$layout->addItem($W->Title('Event on field'));
		
		$editor2 = $this->testEditor();
		$layout->addItem($editor2);
		
		return $layout;
	}
	
	
	/**
	 * 
	 * @return Widget_Form
	 */
	public function testEditor()
	{
	    $W = bab_Widgets();
	    $editor = $W->Form();
	    
	    $editor->setHiddenValue('tg', bab_rp('tg'));
	    $editor->addItem($W->LabelledWidget(
	        bab_isUserLogged() ? 'User logged in' : 'User logged out',
	        $ck = $W->Checkbox(),
	        'mycheck'
	    ));
	    $editor->addItem($W->SubmitButton()->setLabel('Save'));
	    
	    $editor->testField = $ck;
	    
	    $editor->setAjaxAction($this->proxy()->formCheck(), null, 'change');
	    
	    return $editor;
	}
	
	
	
	public function formCheck()
	{
	    bab_requireCredential();
	    header('Content-type: application/json');
	    
	    echo bab_json_encode(array(
	        'messages' => 
	        array(
	            array(
    	           'level' => 'info',
    	           'content' => 'Form checked'
	            )
    	    )
	    ));
	    
	    die();
	}
	
	
	public function loginRequired()
	{
		bab_requireCredential();
		
		$W = bab_Widgets();
		$page = $W->babPage();
		
		$page->addItem($W->Label('Restricted page'));
		
		return $page;
	}
}