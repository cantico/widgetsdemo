<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlPortletDemo extends widgetsDemo_Controller
{


    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Simple portlet container'),
                $W->VBoxItems(
                    $this->portletContainer1()
                ),
                3
            )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlPortletDemo::portletContainer1'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('2-columns portlet container'),
                $W->VBoxItems(
                    $this->portletContainer2()
                    ),
                3
                )->setFoldable(true)
            );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlPortletDemo::portletContainer2'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('3-columns portlet container'),
                $W->VBoxItems(
                    $this->portletContainer3()
                    ),
                3
                )->setFoldable(true)
            );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlPortletDemo::portletContainer3'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('4-columns portlet container'),
                $W->VBoxItems(
                    $this->portletContainer4()
                    ),
                3
                )->setFoldable(true)
            );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlPortletDemo::portletContainer4'));

        return $box;
    }



    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
        	return $box;
        }

        $W = bab_Widgets();

        $page = $W->BabPage(null, $box);

        $page->setTitle(widgetsDemo_translate('Portlets'));

        return $page;
    }



    /**
     * Portlet demo.
     *
     * @return Widget_VBoxLayout
     */
    public function portletContainer1()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $ovml = '<OFPortletContainer classname="portlets-container-100pc" id="widgetdemo_portlets1_1">';
        $html = bab_printOvml($ovml, array());

        $box->addItem(
        	$W->Html($html)
        );

        return $box;
    }


    /**
     * Portlet demo.
     *
     * @return Widget_VBoxLayout
     */
    public function portletContainer2()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $ovml = '<OFPortletContainer classname="portlets-container-50pc" id="widgetdemo_portlets2_1">';
        $ovml .= '<OFPortletContainer classname="portlets-container-50pc" id="widgetdemo_portlets2_2">';
        $html = bab_printOvml($ovml, array());

        $box->addItem(
        	$W->Html($html)
        );

        return $box;
    }


    /**
     * Portlet demo.
     *
     * @return Widget_VBoxLayout
     */
    public function portletContainer3()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $ovml = '<OFPortletContainer classname="portlets-container-33pc" id="widgetdemo_portlets3_1">';
        $ovml .= '<OFPortletContainer classname="portlets-container-33pc" id="widgetdemo_portlets3_2">';
        $ovml .= '<OFPortletContainer classname="portlets-container-33pc" id="widgetdemo_portlets3_3">';
        $html = bab_printOvml($ovml, array());

        $box->addItem(
        	$W->Html($html)
        );

        return $box;
    }


    /**
     * Portlet demo.
     *
     * @return Widget_VBoxLayout
     */
    public function portletContainer4()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        $ovml = '<OFPortletContainer classname="portlets-container-25pc" id="widgetdemo_portlets4_1">';
        $ovml .= '<OFPortletContainer classname="portlets-container-25pc" id="widgetdemo_portlets4_2">';
        $ovml .= '<OFPortletContainer classname="portlets-container-25pc" id="widgetdemo_portlets4_3">';
        $ovml .= '<OFPortletContainer classname="portlets-container-25pc" id="widgetdemo_portlets4_4">';
        $html = bab_printOvml($ovml, array());

        $box->addItem(
        	$W->Html($html)
        );

        return $box;
    }
}
