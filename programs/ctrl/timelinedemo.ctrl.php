<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlTimelineDemo extends widgetsDemo_Controller
{
    
        
    public function demo()
    {
    	$W = bab_Widgets();
    	
    	$box = $W->VBoxLayout();
    	$box->setVerticalSpacing(1, 'em');
    	
    	$box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Sample timeline'),
        		$W->VBoxItems(
        		    $this->timeline()
				),
        		3
    	    )->setFoldable(true)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlTimelineDemo::timeline'));
        
        

        return $box;
    }
    
    

    public function display()
    {
        $box = $this->demo();
        if (bab_isAjaxRequest()) {
        	return $box;
        }

        $W = bab_Widgets();
        
        $page = $W->BabPage(null, $box);
        
        $page->setTitle(widgetsDemo_translate('Timelines'));
        
        return $page;
    }





    /**
     * Timeline demo.
     *
     * @return Widget_VBoxLayout
     */
    public function timeline()
    {
    	$W = bab_Widgets();
    	 
    	$box = $W->VBoxItems();
    	$box->setVerticalSpacing(1, 'em');


    	// A simple timeline.
    	//---------------------------------------------------------------------
    	$timeline = $W->Timeline();
    	
    	$timeline->setStart(BAB_DateTime::fromIsoDateTime('2014-01-01 00:00:00'));
    	$timeline->setEnd(BAB_DateTime::fromIsoDateTime('2015-01-01 00:00:00'));
    	 
    	$timeline->addBand(new Widget_TimelineBand('80%', Widget_TimelineBand::INTERVAL_WEEK, 36 * 7, Widget_TimelineBand::TYPE_ORIGINAL));
    	$timeline->addBand(new Widget_TimelineBand('20%', Widget_TimelineBand::INTERVAL_MONTH, 150, Widget_TimelineBand::TYPE_OVERVIEW));
    	
    	$box->addItem(
    		$timeline
    	);

    	return $box;
    }
     
}
