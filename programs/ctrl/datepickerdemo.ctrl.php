<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';


/**
 *
 */
class widgetsDemo_CtrlDatePickerDemo extends widgetsDemo_Controller
{



    public function demo()
    {
        $W = bab_Widgets();

        $box = $W->VBoxLayout();
        $box->setVerticalSpacing(1, 'em');


        $box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Date pickers'),
        		$W->VBoxItems(
        		    $this->datePickers()
				),
        		3
    	    )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlDatePickerDemo::datePickers'));

        $box->addItem(
        	$section = $W->Section(
        		widgetsDemo_translate('Inline date pickers'),
        		$W->VBoxItems(
        		    $this->inlineDatePickers()
				),
        		3
    	    )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlDatePickerDemo::inlineDatePickers'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Datetime pickers'),
                $W->VBoxItems(
                    $this->dateTimePickers()
                ),
                3
            )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlDatePickerDemo::dateTimePickers'));

        $box->addItem(
            $section = $W->Section(
                widgetsDemo_translate('Period pickers'),
                $W->VBoxItems(
                    $this->periodPickers()
                ),
                3
            )->setFoldable(true, false)
        );
        $section->addContextMenu()->addItem($this->codeSection('widgetsDemo_CtrlDatePickerDemo::periodPickers'));

        return $box;
    }


    public function display()
    {
    	$box = $this->demo();
    	if (bab_isAjaxRequest()) {
    		return $box;
    	}

    	$W = bab_Widgets();

    	$page = $W->BabPage(null, $box);

    	$page->setTitle(widgetsDemo_translate('Date picker widgets'));

    	return $page;
    }


    /**
     * Inline date pickers demo.
     *
     * @return Widget_VBoxLayout
     */
    public function inlineDatePickers()
    {
        $W = bab_Widgets();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');


        // An inline date picker displaying 6 (3x2) months at once.
        //---------------------------------------------------------------------
        $datePicker1 = $W->DatePicker();
        $datePicker1->setNumberOfMonths(3, 1);
        $datePicker1->setInline(true);
        $datePicker1->setHighlightedDate('2015-10-10');
        $box->addItem(
            $this->labelledItem('Multimonth inline date picker', $datePicker1)
        );

        $start = BAB_DateTime::now();
        $start->add(-5, BAB_DATETIME_DAY);

        $end = BAB_DateTime::now();
        $end->add(+5, BAB_DATETIME_DAY);

        // An inline date picker displaying 6 (3x2) months at once.
        //---------------------------------------------------------------------
        $datePicker2 = $W->DatePicker();
        $datePicker2->setNumberOfMonths(3, 1);
        $datePicker2->setInline(true);
        $datePicker2->setHighlightedDate($start, $end);
        $box->addItem(
            $this->labelledItem('Multimonth inline date picker', $datePicker2)
        );

        return $box;
    }


    /**
     * Date pickers demo.
     *
     * @return Widget_VBoxLayout
     */
    public function datePickers()
    {
    	$W = bab_Widgets();

    	$box = $W->VBoxItems();
    	$box->setVerticalSpacing(1, 'em');


    	// A simple date picker with default configuration.
    	//---------------------------------------------------------------------
		$datePicker1 = $W->DatePicker();
    	$box->addItem(
    		$this->labelledItem('Default date picker', $datePicker1)
    	);


    	// A date picker displaying 6 (3x2) months at once.
    	//---------------------------------------------------------------------
		$datePicker2 = $W->DatePicker();
    	$datePicker2->setNumberOfMonths(3, 2);
    	$box->addItem(
    		$this->labelledItem('Multimonth date picker', $datePicker2)
    	);


    	// A date picker with a lower limit.
    	//---------------------------------------------------------------------
		$datePicker3 = $W->DatePicker();
    	$datePicker3->setMinDate(BAB_DateTime::now());
    	$box->addItem(
    		$this->labelledItem('Date picker with min date', $datePicker3)
    	);



    	// A date picker with a lower limit.
    	//---------------------------------------------------------------------
    	$datePicker4 = $W->DatePicker();
    	$datePicker4->setYearSelectable(true);
    	$datePicker4->setMonthSelectable(true);
    	$datePicker4->showWeek(true);
    	$datePicker4->setNumberOfMonths(3, 2);
    	$box->addItem(
    	    $this->labelledItem('Multimonth date picker with selectable month and year, week number visible', $datePicker4)
    	);

        return $box;
    }

	/**
	 * Datetime pickers demo.
	 *
	 * @return Widget_VBoxLayout
	 */
	public function dateTimePickers()
	{
	    $W = bab_Widgets();

	    $box = $W->VBoxItems();
	    $box->setVerticalSpacing(1, 'em');


	    // A date and time picker.
	    //---------------------------------------------------------------------
	    $dateTimePicker = $W->DateTimePicker();
	    $box->addItem(
	        $this->labelledItem('Default datetime picker', $dateTimePicker)
	    );


    	// A date and time picker.
    	//---------------------------------------------------------------------
    	$dateTimePicker2 = $W->DateTimePicker();
    	$dateTimePicker2->setYearSelectable(true);
    	$dateTimePicker2->setMonthSelectable(true);
    	$dateTimePicker2->showWeek(true);
    	$dateTimePicker2->setNumberOfMonths(3, 2);
    	$box->addItem(
    	    $this->labelledItem('Multimonth datetime picker with selectable month and year, week number visible', $dateTimePicker2)
    	);

    	return $box;
	}


	/**
	 * Period pickers demo.
	 *
	 * @return Widget_VBoxLayout
	 */
	public function periodPickers()
	{
	    $W = bab_Widgets();

	    $box = $W->VBoxItems();
	    $box->setVerticalSpacing(1, 'em');


	    // A period picker displays 2 linked date pickers.
	    // Min and max values despends on one another.
	    //---------------------------------------------------------------------
	    $periodPicker = $W->PeriodPicker();
	    $box->addItem(
	        $this->labelledItem('Default period picker', $periodPicker)
	    );


        // A multimonth period picker.
	    //---------------------------------------------------------------------
		$periodPicker2 = $W->PeriodPicker();
    	$periodPicker2->setNumberOfMonths(4, 3);
    	$periodPicker2->setYearSelectable(true);
    	$periodPicker2->setMonthSelectable(true);
    	$periodPicker2->showWeek(true);
    	$box->addItem(
    		$this->labelledItem('Multimonth period picker', $periodPicker2)
    	);

        // A multimonth datetime period picker.
	    //---------------------------------------------------------------------
		$periodPicker3 = $W->PeriodPicker(null, true);
    	$periodPicker3->setNumberOfMonths(4, 3);
    	$periodPicker3->setYearSelectable(true);
    	$periodPicker3->setMonthSelectable(true);
    	$periodPicker3->showWeek(true);
    	$box->addItem(
    		$this->labelledItem('Multimonth datetime period picker', $periodPicker3)
    	);

    	return $box;
    }
}
