<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

if (!function_exists('bab_isAjaxRequest')) {
    function bab_isAjaxRequest()
    {
        $isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
        return $isAjaxRequest;
    }
}


/**
 * @return Func_Widgets
 */
function Demo_widgetFactory()
{
	$W = bab_functionality::get('Widgets', false);
	$W->includeCss();

	return $W;
}




/**
 * @return widgetsDemo_Controller
 */
function widgetsDemo_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('widgetsDemo_Controller');
}


/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function widgetsDemo_translate($str)
{
	$translation = bab_translate($str, 'widgetsDemo');

	return $translation;
}

/**
 * Returns the php code of a method in a string.
 *
 * @param string $method
 * @return string
 */
function widgetsDemo_methodCode($method)
{
    $method = new ReflectionMethod($method);
    $filename = $method->getFileName();
    $start_line = $method->getStartLine() - 1; // it's actually - 1, otherwise you wont get the function() block
    $end_line = $method->getEndLine();
    $length = $end_line - $start_line;
    	
    $source = file($filename);
    $php = implode('', array_slice($source, $start_line, $length));
    	
    return $php;
}




/**
 * Initialize mysql ORM backend.
 */
function widgetsDemo_loadOrm()
{
	static $loadOrmDone = false;

	if (!$loadOrmDone) {

		$Orm = bab_functionality::get('LibOrm');
		/*@var $Orm Func_LibOrm */
		$Orm->initMySql();

		$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
		ORM_MySqlRecordSet::setBackend($mysqlbackend);

		$loadOrmDone = true;
	}
}